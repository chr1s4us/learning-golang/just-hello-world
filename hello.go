package main

import (
	"fmt"

	"rsc.io/quote"
)

func main() {
	fmt.Println("Hi! I am your very first go application!")
	fmt.Println("Some sting" + "testing")
	fmt.Println(1 + 1)
	fmt.Println(quote.Go())
}
